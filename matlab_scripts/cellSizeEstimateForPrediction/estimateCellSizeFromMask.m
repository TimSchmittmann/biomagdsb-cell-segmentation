function [median_size, std_size] = estimateCellSizeFromMask(maskImg)

props = regionprops(maskImg, 'EquivDiameter'); 
allSize = [props.EquivDiameter];    

if(isempty(allSize))
    median_size = 1; % NaN;
else 
    median_size = median(allSize);
endif

std_size = std(allSize);
