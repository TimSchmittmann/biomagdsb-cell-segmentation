#!/bin/bash

# vars from params.sh:
ROOT_DIR="$1"
IMAGES_DIR="$2"

if [ $# -eq 3 ]
then
    export PYTHONPATH=$PYTHONPATH:$3
else
    export PYTHONPATH=$PYTHONPATH:"Mask_RCNN-2.1"
fi


WORKFLOW_ROOT="$ROOT_DIR/kaggle_workflow"

MASKRCNN_SCRIPTS="$ROOT_DIR/FinalModel"
MASKRCNN="$WORKFLOW_ROOT/maskrcnn"
MATLAB_SCRIPTS="$ROOT_DIR/matlab_scripts"
OUTPUTS_DIR="$WORKFLOW_ROOT/outputs"
CELLSIZE_EST="$OUTPUTS_DIR/cellSizeEstimator"

INPUT_IMAGES="$OUTPUTS_DIR/images"
INPUT_IMAGES_2X="$OUTPUTS_DIR/2ximages/images"
UNET="$WORKFLOW_ROOT/unet"
UNET_OUT="$OUTPUTS_DIR/unet_out"
UNET_SCRIPTS="$ROOT_DIR/UNet"
MASS_TRAIN_UNET="$OUTPUTS_DIR/train_unet"
VALIDATION="$OUTPUTS_DIR/validation"
ENSEMBLE="$OUTPUTS_DIR/ensemble"
POST_PROCESSING="$OUTPUTS_DIR/postprocessing"

# check inputs:
echo "ROOT_DIR: " $ROOT_DIR
echo "WORKFLOW_ROOT: " $WORKFLOW_ROOT
echo "MASKRCNN_SCRIPTS: " $MASKRCNN_SCRIPTS
echo "MASKRCNN: " $MASKRCNN
echo "MATLAB_SCRIPTS: " $MATLAB_SCRIPTS
echo "OUTPUTS_DIR: " $OUTPUTS_DIR
echo "CELLSIZE_EST: " $CELLSIZE_EST
echo "IMAGES_DIR: " $IMAGES_DIR

echo "POSTPROCESSING:"
octave --no-gui --eval "pkg load image;addpath('${MATLAB_SCRIPTS}/postProcess');postProcCodeRunnerFINAL('${OUTPUTS_DIR}/maskrcnn/','${POST_PROCESSING}/','2x/','4x/','','master','${ENSEMBLE}/output/','final',false,'${INPUT_IMAGES}',[]);exit;"
if [ $? -ne 0 ]
then
    echo ERROR: "Error during postprocessing"
    exit 1
fi
echo "POSTPROCESSING DONE"


# --- delete dummy folders ---
rm -r $MASS_TRAIN_UNET
